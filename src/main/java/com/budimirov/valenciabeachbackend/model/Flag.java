package com.budimirov.valenciabeachbackend.model;

public enum Flag {
    GREEN, YELLOW, RED
}
