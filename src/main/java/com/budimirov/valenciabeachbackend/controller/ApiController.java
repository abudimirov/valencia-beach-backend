package com.budimirov.valenciabeachbackend.controller;

import com.budimirov.valenciabeachbackend.entity.Beach;
import com.budimirov.valenciabeachbackend.repository.BeachRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
public class ApiController {

    private final BeachRepository beachRepository;

    @GetMapping
    public ResponseEntity<List<Beach>> getBeachData() {
        return new ResponseEntity<>(beachRepository.findAll(), HttpStatus.OK);
    }
}
