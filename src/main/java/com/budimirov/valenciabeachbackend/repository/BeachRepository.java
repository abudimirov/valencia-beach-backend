package com.budimirov.valenciabeachbackend.repository;


import com.budimirov.valenciabeachbackend.entity.Beach;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeachRepository extends JpaRepository<Beach, Long> {
    Beach findByName(String name);
    List<Beach> findAll();
}
