package com.budimirov.valenciabeachbackend.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Beach {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String currentFlag;
    private boolean isJellyFishPresent;
    private String swellSize;
    private String airTemp;
    private String airSpeed;
    private String airDirection;
    private String waterTemp;
    private String beachCapacity;
    private int uvIndex;
    private boolean isActiveLifeguard;
    private LocalDateTime updatedAt;
}
