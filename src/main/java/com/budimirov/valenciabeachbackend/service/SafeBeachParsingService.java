package com.budimirov.valenciabeachbackend.service;

import com.budimirov.valenciabeachbackend.exception.TechnicalException;
import com.budimirov.valenciabeachbackend.repository.BeachRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class SafeBeachParsingService {
    @Value("${safeBeach.url}")
    private final String safeBeachUrl;
    private final BeachRepository beachRepository;

    @Scheduled(fixedDelay = 3600000, initialDelay = 1000)
    public void parse() {
        try {
            final var webPage = Jsoup.connect(safeBeachUrl).get();
            updateBeachList(webPage.select(".col-centered"));
        } catch (IOException e) {
            throw new TechnicalException(e.getMessage());
        }
    }

    private void updateBeachList(final Elements cards) {
        if (cards == null) throw new TechnicalException("Parsing error. Got empty list of beaches by css class .col-centered");

        for (final Element card: cards) {
            final var description = card.select(".feature_text");
            final var beachName = description.select("h3").text().trim();

            if (card.text().toLowerCase().contains("waiting for beach data")) {
                log.info(String.format("No actual data is found for beach %s. Stay with old data in db", beachName));
                return;
            }

            final var flag = card.select(".bandera_texto").text(); //TODO: replace with ENUM
            final var isActiveLifeguard = !card.select(".cajasuperior").text()
                    .toLowerCase()
                    .contains("not active");
            final var allText = card.select(".cajatexto").text();
            final var isJellyFishPresent = StringUtils.substringBetween(allText, "Presence of jellyfish:", "\uE806")
                    .toLowerCase()
                    .trim()
                    .equals("yes");
            final var swellSize = StringUtils.substringBetween(allText, "Swell:", "\uE809")
                    .toLowerCase()
                    .trim();
            final var beachCapacity = StringUtils.substringBetween(allText, "Influx of people:", "Capacity").trim();
            final var waterTemp = StringUtils.substringBetween(allText, "Water temperature:", "\uE80A").trim();
            final var uvIndex = Integer.parseInt(allText.split("UV radiation index:")[1].trim());

            final var beach = beachRepository.findByName(beachName);
            beach.setCurrentFlag(flag);
            beach.setUpdatedAt(LocalDateTime.now()); //TODO: add time formatter
            beach.setActiveLifeguard(isActiveLifeguard);
            beach.setJellyFishPresent(isJellyFishPresent);
            beach.setSwellSize(swellSize);
            beach.setBeachCapacity(beachCapacity);
            beach.setWaterTemp(waterTemp);
            beach.setUvIndex(uvIndex);
            beachRepository.save(beach);
        }
    }
}
